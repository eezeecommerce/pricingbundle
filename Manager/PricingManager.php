<?php

namespace eezeecommerce\PricingBundle\Manager;
use Doctrine\ORM\PersistentCollection;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use eezeecommerce\PricingBundle\Utils\Pricing;
use eezeecommerce\ProductBundle\Entity\Options;
use eezeecommerce\ProductBundle\Entity\Product;
use eezeecommerce\ProductBundle\Entity\Variants;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use eezeecommerce\ShippingBundle\Entity\CourierServicePricing;
use eezeecommerce\UserBundle\Entity\Groups;
use eezeecommerce\UserBundle\Entity\GroupsPricing;
use eezeecommerce\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PricingManager
 * @package eezeecommerce\PricingBundle\Manager
 */
class PricingManager
{

    /**
     * Instance of SettingsProvider
     *
     * @var Settings
     */
    protected $settings;

    /**
     * Instance of Pricing Class
     *
     * @var Pricing
     */
    protected $pricing;

    /**
     * @var TokenStorage
     */
    protected $token;

    /**
     * @var float
     */
    protected $defaultCurrency;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * Set Token Storage
     *
     * @param TokenStorage $token TokenStorage
     *
     * @return void
     */
    public function setToken(TokenStorage $token)
    {
        $this->token = $token;
    }

    /**
     * Set Settings
     *
     * @param SettingsProvider $settings Set Settings
     *
     * @return void
     */
    public function setSettings(SettingsProvider $settings)
    {
        $this->settings = $settings->loadSettingsBySite();
    }

    public function setCurrency(CurrencyProvider $currency)
    {
        $this->currency = $currency->loadCurrency()->getEntity();
    }

    /**
     * Set instance of pricing methods
     *
     * @param Pricing $pricing Pricing Class
     *
     * @return void
     */
    public function setPricing(Pricing $pricing)
    {
        $this->pricing = $pricing;
    }

    /**
     *
     */
    public function getProductPrice(Product $product)
    {
        $user = $this->token->getToken()->getUser();

        $product->setBasePrice($this->currencyConversion($product->getBasePrice()));

        foreach ($product->getOptions() as $option) {
            if (!$option instanceof Options) {
                throw new \InvalidArgumentException(sprintf("Option Should be an instance of Option Entity instead instance of % was passed through", get_class($option)));
            }
            $option->setBasePrice($this->currencyConversion($option->getBasePrice()));
        }

        foreach ($product->getVariants() as $variant) {
            if (!$variant instanceof Variants) {
                throw new \InvalidArgumentException(sprintf("Variant Should be an instance of Variant Entity instead instance of % was passed through", get_class($variant)));
            }

            $variant->setBasePrice($this->currencyConversion($variant->getBasePrice()));
        }

        if (!$user instanceof User)
        {
            return $product;
        }

        if (count($user->getGrouping()) < 1) {
            return $product;
        }

        foreach ($user->getGrouping() as $group) {
            if (!$group instanceof Groups) {
                throw new \InvalidArgumentException(sprintf("Group Should be an instance of Group Entity instead instance of % was passed through", get_class($group)));
            }

            if (count($group->getPricing()) > 0) {
                if ($product->getBasePrice() != "0.0000") {
                    $product->setBasePrice($this->pricingSchemeConversion($product->getBasePrice(), $group->getPricing()));
                }
                foreach ($product->getOptions() as $option) {
                    if (!$option instanceof Options) {
                        throw new \InvalidArgumentException(sprintf("Option Should be an instance of Option Entity instead instance of % was passed through", get_class($option)));
                    }
                    if ($option->getBasePrice() == "0.0000") {
                        continue;
                    }
                    $option->setBasePrice($this->pricingSchemeConversion($option->getBasePrice(), $group->getPricing()));
                }

                foreach ($product->getVariants() as $variant) {
                    if (!$variant instanceof Variants) {
                        throw new \InvalidArgumentException(sprintf("Variant Should be an instance of Variant Entity instead instance of % was passed through", get_class($variant)));
                    }
                    if ($variant->getBasePrice() == "0.0000") {
                        continue;
                    }

                    $variant->setBasePrice($this->pricingSchemeConversion($variant->getBasePrice(), $group->getPricing()));
                }

            }
        }

        return $product;
    }

    /**
     * @param array $courierPricingArray
     * @return array
     */
    protected function getShippingPrice(array $courierPricingArray)
    {
        foreach ($courierPricingArray as $pricing) {
            if (!$pricing instanceof CourierServicePricing) {
                throw new \InvalidArgumentException(sprintf("Pricing should be an instance of CourierServicePricing entity. Instead, %s was used", get_class($pricing)));
            }
            $pricing->setBasePrice($this->currencyConversion($pricing->getBasePrice()));
        }

        return $courierPricingArray;
    }

    /**
     * Initialise currency conversion based on default currency conversion rate
     *
     * @param float $price
     *
     * @return float
     */
    protected function currencyConversion($price)
    {
        if (null === $this->currency) {

            if (!$this->currency instanceof Currency) {
                throw new \InvalidArgumentException(sprintf("Default Currency Should be an instance of Currency instead instance of % was passed through", get_class($this->currency)));
            }

            if (null === $this->currency->getExchangeRate() || $this->currency->getExchangeRate() === '0.000000') {
                throw new \InvalidArgumentException("Exchange rate must be greater than 0");
            }
        }

        $exchangeRate = $this->currency->getExchangeRate();

        return round($price * $exchangeRate, 4);
    }

    protected function pricingSchemeConversion($price, PersistentCollection $pricingScheme)
    {
        $this->pricing->setBasePrice($price);
        foreach ($pricingScheme as $scheme) {
            if (!$scheme instanceof GroupsPricing) {
                throw new \InvalidArgumentException("GroupsPricing Should be an instance of GroupsPricing instead instance of % was passed through", get_class($scheme));
            }

            $amount = $scheme->getAmount();

            switch ($scheme->getMethod()) {
                case "ADD":
                    $this->pricing->add($amount);
                    break;
                case "SUBTRACT":
                    $this->pricing->subtract($amount);
                    break;
                case "MULTIPLY":
                    $this->pricing->multiply($amount);
                    break;
                case "DIVIDE":
                    $this->pricing->divide($amount);
                    break;
                case "ROUNDUP":
                    $this->pricing->round($amount, Pricing::ROUND_UP);
                    break;
                case "ROUNDDOWN":
                    $this->pricing->round($amount, Pricing::ROUND_DOWN);
                    break;
            }
            $newPrice = $this->pricing->getPrice();
            $this->pricing->setBasePrice($newPrice);
        }
        return $this->pricing->getPrice();
    }

    public function resetBasePrice(Product $originalEntity, Product $cartEntity)
    {
        $cartEntity->setBasePrice($originalEntity->getBasePrice());

        foreach ($cartEntity->getOptions() as $key => $option) {
            if (!$option instanceof Options) {
                throw new \InvalidArgumentException(sprintf("Option Should be an instance of Option Entity instead instance of % was passed through", get_class($option)));
            }
            $option->setBasePrice($originalEntity->getOptions()->get($key)->getBasePrice());
        }

        foreach ($cartEntity->getVariants() as $key => $variant) {
            if (!$variant instanceof Variants) {
                throw new \InvalidArgumentException(sprintf("Variant Should be an instance of Variant Entity instead instance of % was passed through", get_class($variant)));
            }

            $variant->setBasePrice($originalEntity->getVariants()->get($key)->getBasePrice());
        }
    }
}