<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26/04/16
 * Time: 10:06
 */

namespace eezeecommerce\PricingBundle\Utils;


class Pricing
{

    const ROUND_DOWN = "DOWN";

    const ROUND_UP = "UP";

    /**
     * @var float
     */
    protected $price;

    /**
     * @param $price
     */
    public function setBasePrice($price)
    {
        $this->price = $price;
    }

    public function round($increments = 0.50, $method = SELF::ROUND_UP)
    {
        if ($method != "UP" && $method != "DOWN") {
            throw new \InvalidArgumentException(sprintf("Method should be either UP or Down instead %s was used.", $method));
        }

        if ($increments > 1) {
            throw new \InvalidArgumentException(sprintf("Increments have to be equal or less than 1. Instead %s was used", $increments));
        }

        if ($increments <= 0.5) {
            $fig = (float) (1 / $increments);

            if ($method == "DOWN") {
                $n = floor($this->price * $fig);
            } else {
                $n = ceil($this->price * $fig);
            }

            $this->price = $n / $fig;
        } else {
            $fig = 1;

            if ($method == "DOWN") {
                $n = floor($this->price * $fig);
                if ($this->price < 1) {
                    $this->price = ($n / $fig) + $increments;
                } else {
                    $this->price = ($n / $fig) - ($fig - $increments);
                }

            } else {
                $n = ceil($this->price * $fig);
                if ($this->price < 1) {
                    $this->price = ($n / $fig) + $increments;
                } else {
                    $this->price = ($n / $fig) - ($fig - $increments);
                }
            }
        }
    }

    public function multiply($multiplier)
    {
        if ($multiplier == 0) {
            throw new \InvalidArgumentException("Invalid Operation. Cannot multiply by 0.");
        }
        $this->price = $this->price * $multiplier;
    }

    public function divide($multiplier)
    {
        if ($multiplier == 0) {
            throw new \InvalidArgumentException("Invalid Operation. Cannot divide by 0.");
        }
        $this->price = $this->price / $multiplier;
    }

    public function add($multiplier)
    {
        $this->price = $this->price + $multiplier;
    }

    public function subtract($multiplier)
    {
        $this->price = $this->price - $multiplier;
    }

    public function getPrice()
    {
        if ($this->price < 0) {
            throw new \InvalidArgumentException(sprintf("Price cannot be less than 0."));
        }

        return $this->price;
    }


}