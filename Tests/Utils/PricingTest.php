<?php

namespace eezeecommerce\PricingBundle\Tests\Utils;


use eezeecommerce\PricingBundle\Utils\Pricing;

class PricingTest extends \PHPUnit_Framework_TestCase
{
    public function testRoundUpNearest()
    {
        $price = 10.55;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->round(0.25);

        $this->assertEquals(10.75, $pricing->getPrice());

        $price = 0.75;

        $pricing->setBasePrice($price);

        $pricing->round(0.61);

        $this->assertEquals(1.61, $pricing->getPrice());

        $price = 10.30;

        $pricing->setBasePrice($price);

        $pricing->round(0.50);

        $this->assertEquals(10.50, $pricing->getPrice());

        $pricing->round(0.61);

        $this->assertEquals(10.61, $pricing->getPrice());
    }

    public function testRoundDownNearest()
    {
        $price = 10.55;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->round(0.25, Pricing::ROUND_DOWN);

        $this->assertEquals(10.50, $pricing->getPrice());

        $price = 10.30;

        $pricing->setBasePrice($price);

        $pricing->round(0.50, Pricing::ROUND_DOWN);

        $this->assertEquals(10.00, $pricing->getPrice());

        $pricing->round(0.99, Pricing::ROUND_DOWN);

        $this->assertEquals(9.99, $pricing->getPrice());

        $price = 0.75;

        $pricing->setBasePrice($price);

        $pricing->round(0.61, Pricing::ROUND_DOWN);

        $this->assertEquals(0.61, $pricing->getPrice());

        $price = 10.00;

        $pricing->setBasePrice($price);

        $pricing->round(1, Pricing::ROUND_DOWN);

        $this->assertEquals(10.00, $pricing->getPrice());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUsingValueGreaterThanOneThrowsException()
    {
        $price = 10.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->round(10, Pricing::ROUND_DOWN);

        $this->assertEquals(10.00, $pricing->getPrice());
    }

    public function testMultiplierWorksCorrectly()
    {
        $price = 10.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->multiply(10);

        $this->assertEquals(100.00, $pricing->getPrice());
    }

    public function testDivideGivesCorrectResult()
    {
        $price = 10.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->divide(10);

        $this->assertEquals(1.00, $pricing->getPrice());
    }

    public function testAddGivesCorrectResult()
    {
        $price = 10.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->add(10);

        $this->assertEquals(20.00, $pricing->getPrice());
    }

    public function testSubtractGivesCorrectResult()
    {
        $price = 10.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->subtract(10);

        $this->assertEquals(0.00, $pricing->getPrice());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testPriceLowerThanZeroThrowsException()
    {
        $price = 0.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->subtract(10);

        $pricing->getPrice();
    }

    public function testAllFunctions()
    {
        $price = 0.00;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->add(2.5);

        $this->assertEquals(2.5, $pricing->getPrice());

        $pricing->round(0.5);

        $this->assertEquals(2.5, $pricing->getPrice());

        $pricing->multiply(2);

        $this->assertEquals(5, $pricing->getPrice());

        $pricing->divide(5);

        $this->assertEquals(1, $pricing->getPrice());

        $pricing->subtract(1);

        $this->assertEquals(0.00, $pricing->getPrice());
    }

    public function testMultiplyDivideAndRoundUpTo()
    {
        $price = 1002.53;

        $pricing = new Pricing();

        $pricing->setBasePrice($price);

        $pricing->divide(2);

        $this->assertEquals(501.265, $pricing->getPrice());

        $pricing->multiply(1.5);

        $this->assertEquals(751.8975, $pricing->getPrice());

        $pricing->round(0.01);

        $this->assertEquals(751.90, $pricing->getPrice());
    }
}