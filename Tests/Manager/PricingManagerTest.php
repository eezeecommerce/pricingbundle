<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/05/16
 * Time: 11:32
 */

namespace eezeecommerce\PricingBundle\Tests\Manager;


use eezeecommerce\PricingBundle\Manager\PricingManager;
use eezeecommerce\PricingBundle\Utils\Pricing;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use eezeecommerce\UserBundle\Entity\User;

class PricingManagerTest extends \PHPUnit_Framework_TestCase
{
    private $token = null;

    private $settings = null;

    private $pricing = null;

    public function setUp()
    {
        $token = $this->getMockBuilder("Symfony\Component\Security\Core\Authentication\Token")
            ->disableOriginalConstructor()
            ->getMock();

        $TokenStorage = $this->getMockBuilder("Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage")
            ->disableOriginalConstructor()
            ->getMock();
        $TokenStorage->expects($this->any())
            ->method("getToken")
            ->will($this->returnValue($token));

        $this->token = $TokenStorage;

        $settings = $this->getMockBuilder(Settings::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settingsProvider = $this->getMockBuilder(SettingsProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settingsProvider->expects($this->any())
            ->method("loadSettingsBySite")
            ->will($this->returnValue($settings));

        $this->settings = $settingsProvider;

        $pricing = $this->getMockBuilder(Pricing::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->pricing = $pricing;
    }

    public function testPricingManager()
    {
        $manager = new PricingManager();

        $manager->setToken($this->token);

        $manager->setSettings($this->settings);

        $manager->setPricing($this->pricing);
    }
}