<?php

namespace eezeecommerce\PricingBundle\EventSubscriber;

use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\CartBundle\CartEvents;
use eezeecommerce\CartBundle\Event\FilterItemEvent;
use eezeecommerce\CartBundle\Event\FilterItemsEvent;
use eezeecommerce\PricingBundle\Manager\PricingManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UserEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var PricingManager
     */
    private $manager;

    /**
     * @var ObjectManager
     */
    private $em;

    public function setPricingManager(PricingManager $manager)
    {
        $this->manager = $manager;
    }

    public function setEntityManager(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return array(
            CartEvents::CART_ITEM_ADD_INITIALISE => array(
                array("onItemAdd", 100),
            ),
             CartEvents::CART_UPDATE_PRICING_INITIALISED => array(
                array("onUserLogin", 100)
            )
        );
    }

    public function onItemAdd(FilterItemEvent $event)
    {
        $this->manager->getProductPrice($event->getEntity());
    }

    public function onUserLogin(FilterItemsEvent $event)
    {
        $items = $event->getItems();

        if (count($items) < 1) {
            return;
        }

        foreach ($items as $item) {
            $entity = $this->em->getRepository("eezeecommerceProductBundle:Product")
                ->find($item->getEntity()->getId());
            $this->manager->resetBasePrice($entity, $item->getEntity());
            $this->manager->getProductPrice($item->getEntity());
        }

    }
}